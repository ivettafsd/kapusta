import axios from 'axios';
import { showErrorMessage } from '../components/Toasters';

export const getIncomeCategories = async () => {
  try {
    const data = await axios.get('/transaction/income-categories');
    return data;
  } catch (error) {
    showErrorMessage(error.message);
    return null;
  }
};

export const getExpenseCategories = async () => {
  try {
    const data = await axios.get('/transaction/expense-categories');
    return data;
  } catch (error) {
    showErrorMessage(error.message);
    return null;
  }
};

export const getIncome = async () => {
  try {
    const data = await axios.get('/transaction/income');
    return data;
  } catch (error) {
    showErrorMessage(error.message);
    return null;
  }
};

export const getExpense = async () => {
  try {
    const data = await axios.get('/transaction/expense');
    return data;
  } catch (error) {
    showErrorMessage(error.message);
    return null;
  }
};

export const postIncome = async income => {
  try {
    const data = await axios.post('/transaction/income', income);
    return data;
  } catch (error) {
    showErrorMessage(error.message);
  }
};

export const postExpense = async expense => {
  try {
    const data = await axios.post('/transaction/expense', expense);
    return data;
  } catch (error) {
    showErrorMessage(error.message);
  }
};

export const getTransactionsByPeriod = async date => {
  try {
    const data = await axios.get('/transaction/period-data', {
      params: date,
    });
    return data;
  } catch (error) {
    showErrorMessage(error.message);
  }
};

export const deleteTransaction = async idTransaction => {
  try {
    const data = await axios.delete(`/transaction/${idTransaction}`);
    return data;
  } catch (error) {
    showErrorMessage(error.message);
  }
};
