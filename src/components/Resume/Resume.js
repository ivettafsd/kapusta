import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Loader from '../Loader';
import s from './Resume.module.scss';

const Resume = ({ type }) => {
  const { t } = useTranslation();
  const income = useSelector(state => state.transactions.income);
  const expense = useSelector(state => state.transactions.expense);
  const loading = useSelector(state => state.transactions.loading);
  const currentStatistics =
    income && Object.entries(income.monthsStats).filter(el => el[1] !== 'N/A');
  const [statistics, setStatistics] = useState(currentStatistics);

  useEffect(() => {
    if (type === 'expense' && expense) {
      const statistics = Object.entries(expense.monthsStats).filter(
        el => el[1] !== 'N/A',
      );
      setStatistics(statistics);
    } else {
      if (income) {
        const statistics = Object.entries(income.monthsStats).filter(
          el => el[1] !== 'N/A',
        );
        setStatistics(statistics);
      }
    }
  }, [type, income, expense]);

  return (
    <div className={s.boxResume}>
      <h3>{t('home').summary}</h3>
      {loading ? (
        <Loader />
      ) : (
        <>
          {statistics?.length > 0 ? (
            statistics?.map(el => (
              <div key={el[0]} className={s.row}>
                <span>{t('months')[el[0]]}</span>
                <span>{el[1].toFixed(2)}</span>
              </div>
            ))
          ) : (
            <div className={s.notFound}>?</div>
          )}
        </>
      )}
    </div>
  );
};

export default Resume;
