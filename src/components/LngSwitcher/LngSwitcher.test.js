import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

describe('LngSwitcher', () => {
  it('LngSwitcher click', () => {
    const { container } = render(<input type="checkbox" />);
    const checkbox = container.firstChild;
    expect(checkbox).not.toBeChecked();
    userEvent.click(checkbox);
    expect(checkbox).toBeChecked();
  });
});
