import { format } from 'date-fns';
import sprite from '../../assets/common-sprite.svg';
import s from './DateToday.module.scss';

const DateToday = () => {
  const today = format(new Date(), 'dd.MM.yyyy');
  return (
    <div className={s.date}>
      <svg width="20" height="20">
        <use href={`${sprite}#icon-calendar`}></use>
      </svg>
      <span>{today}</span>
    </div>
  );
};

export default DateToday;
