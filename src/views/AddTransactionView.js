import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import ContainerContent from '../containers/ContainerContent';
import ButtonBack from '../components/ButtonBack';
import AddTransactionForm from '../components/AddTransactionForm';
import s from './AddTransactionView.module.scss';

const AddTransactionView = () => {
  const { state } = useLocation();
  const incomeCategories = useSelector(
    state => state.transactions.incomeCategories,
  );
  const expenseCategories = useSelector(
    state => state.transactions.expenseCategories,
  );

  const [categories, setCategories] = useState();

  useEffect(() => {
    if (state.typeTransaction === 'income') {
      setCategories(incomeCategories);
    } else {
      setCategories(expenseCategories);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);

  return (
    <div>
      <ContainerContent>
        <div className={s.content}>
          <ButtonBack />
          {categories && <AddTransactionForm type={state.typeTransaction} />}
        </div>
      </ContainerContent>
    </div>
  );
};

export default AddTransactionView;
