import axios from 'axios';
import { store } from '../redux';
import { refresh } from '../redux/authOperations';

axios.defaults.baseURL = 'https://kapusta-backend.goit.global';

export * from './auth';
export * from './transactions';

axios.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  },
);

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.status === 401) {
      store.dispatch(refresh());
    }

    return Promise.reject(error);
  },
);
