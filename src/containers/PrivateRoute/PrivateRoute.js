import { Navigate, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import routes from '../../routes';

const PrivateRoute = ({ component }) => {
  const location = useLocation();

  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

  if (isAuthenticated) {
    return component;
  }

  return <Navigate to={routes.auth} state={{ from: location }} replace />;
};

export default PrivateRoute;
