import logo from './logo.png';

const Logo = () => {
  return <img src={logo} alt="Kapu$ta logo" width="90" height="31" />;
};

export default Logo;
