import { render, screen } from '@testing-library/react';
import Logo from './Logo';

describe('Logo', () => {
  it('renders Logo component', () => {
    render(<Logo />);
    expect(screen.getByAltText('Kapu$ta logo')).toBeInTheDocument();
  });
});
