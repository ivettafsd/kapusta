import { configureStore, combineReducers } from '@reduxjs/toolkit';
import transactionsReducer from './transactionsSlice';
import authReducer from './authSlice';

const rootReducer = combineReducers({
  auth: authReducer,
  transactions: transactionsReducer,
});

const store = configureStore({
  reducer: rootReducer,
});

export { store };
