import { createSlice } from '@reduxjs/toolkit';
import authOperations from './authOperations';
import transactionsOperations from './transactionsOperations';

const setError = (state, action) => {
  state.loading = false;
  state.error = action.payload;
};

const setLoading = state => {
  state.loading = true;
  state.error = null;
};

const transactionsSlice = createSlice({
  name: 'transactions',
  initialState: {
    expenseCategories: [],
    incomeCategories: [],
    expense: null,
    income: null,
    allTransactions: [],
    transactionsByPeriod: null,
    incomesByPeriod: [],
    expensesByPeriod: [],
    loading: false,
    error: null,
  },
  reducers: {},
  extraReducers: {
    [transactionsOperations.fetchIncome.pending]: setLoading,
    [transactionsOperations.fetchIncome.fulfilled]: (state, action) => {
      state.loading = false;
      state.income = action.payload;
    },
    [transactionsOperations.fetchIncome.rejected]: setError,
    [transactionsOperations.fetchExpense.pending]: setLoading,
    [transactionsOperations.fetchExpense.fulfilled]: (state, action) => {
      state.loading = false;
      state.expense = action.payload;
    },
    [transactionsOperations.fetchExpense.rejected]: setError,
    [transactionsOperations.fetchIncomeCategories.pending]: setLoading,
    [transactionsOperations.fetchIncomeCategories.fulfilled]: (
      state,
      action,
    ) => {
      state.loading = false;
      state.incomeCategories = action.payload;
    },
    [transactionsOperations.fetchIncomeCategories.rejected]: setError,
    [transactionsOperations.fetchExpenseCategories.pending]: setLoading,
    [transactionsOperations.fetchExpenseCategories.fulfilled]: (
      state,
      action,
    ) => {
      state.loading = false;
      state.expenseCategories = action.payload;
    },
    [transactionsOperations.fetchExpenseCategories.rejected]: setError,
    [transactionsOperations.fetchTransactionsByPeriod.pending]: setLoading,
    [transactionsOperations.fetchTransactionsByPeriod.fulfilled]: (
      state,
      action,
    ) => {
      state.loading = false;
      state.transactionsByPeriod = action.payload;
      state.incomesByPeriod = action.payload.incomes.incomesData
        ? Object.entries(action.payload.incomes.incomesData).sort(
            (a, b) => b[1].total - a[1].total,
          )
        : null;
      state.expensesByPeriod = action.payload.expenses.expensesData
        ? Object.entries(action.payload.expenses.expensesData)?.sort(
            (a, b) => b[1].total - a[1].total,
          )
        : null;
    },
    [transactionsOperations.fetchTransactionsByPeriod.rejected]: setError,
    [transactionsOperations.createIncome.pending]: setLoading,
    [transactionsOperations.createIncome.fulfilled]: (state, action) => {
      state.loading = false;
      state.income.incomes.push(action.payload.transaction);
    },
    [transactionsOperations.createIncome.rejected]: setError,
    [transactionsOperations.createExpense.pending]: setLoading,
    [transactionsOperations.createExpense.fulfilled]: (state, action) => {
      state.loading = false;
      state.expense.expenses.push(action.payload.transaction);
    },
    [transactionsOperations.createExpense.rejected]: setError,
    [authOperations.getCurrentUser.pending]: setLoading,
    [authOperations.getCurrentUser.fulfilled]: (state, action) => {
      state.loading = false;
      state.allTransactions = action.payload.transactions;
    },
    [authOperations.getCurrentUser.rejected]: setError,
    [authOperations.logOut.fulfilled]: (state, action) => {
      state.expenseCategories = [];
      state.incomeCategories = [];
      state.expense = null;
      state.income = null;
      state.allTransactions = [];
      state.transactionsByPeriod = null;
      state.incomesByPeriod = [];
      state.expensesByPeriod = [];
      state.loading = false;
      state.error = null;
    },
    [authOperations.refresh.rejected]: state => {
      state.expenseCategories = [];
      state.incomeCategories = [];
      state.expense = null;
      state.income = null;
      state.allTransactions = [];
      state.transactionsByPeriod = null;
      state.incomesByPeriod = [];
      state.expensesByPeriod = [];
      state.loading = false;
      state.error = null;
    },
  },
});

export default transactionsSlice.reducer;
