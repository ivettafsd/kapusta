import CircularProgress from '@mui/material/CircularProgress';
import s from './Loader.module.scss';

const Loader = ({ style, size = 40 }) => {
  return (
    <div className={s.content} style={{ ...style }}>
      <CircularProgress color="primary" size={size} />
    </div>
  );
};

export default Loader;
