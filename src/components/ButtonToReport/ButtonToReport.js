import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import routes from '../../routes';
import sprite from '../../assets/common-sprite.svg';
import s from './ButtonToReport.module.scss';

const ButtonToReport = () => {
  const { t } = useTranslation();
  return (
    <Link to={routes.report} className={s.reports}>
      {t('home').report}
      <svg className={s.icon} width="25" height="25">
        <use href={`${sprite}#icon-chart`}></use>
      </svg>
    </Link>
  );
};

export default ButtonToReport;
