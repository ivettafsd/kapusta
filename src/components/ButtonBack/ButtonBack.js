import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import sprite from '../../assets/common-sprite.svg';
import s from './ButtonBack.module.scss';

const ButtonBack = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();

  return (
    <div className={s.back}>
      <button type="button" onClick={() => navigate('/')} className={s.backBtn}>
        <svg className={s.icon} width="24" height="24">
          <use href={`${sprite}#icon-back`}></use>
        </svg>
        <span className={s.text}>{t('report').back}</span>
      </button>
    </div>
  );
};

export default ButtonBack;
