import { createSlice } from '@reduxjs/toolkit';
import authOperations from './authOperations';

const setError = (state, action) => {
  state.loading = false;
  state.error = action.payload;
};

const setLoading = state => {
  state.loading = true;
  state.error = null;
};

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isAuthenticated: false,
    accessToken: null,
    sid: null,
    refreshToken: null,
    user: null,
    loading: false,
    error: null,
  },
  reducers: {},
  extraReducers: {
    [authOperations.logIn.pending]: setLoading,
    [authOperations.logIn.fulfilled]: (state, action) => {
      state.isAuthenticated = true;
      state.accessToken = action.payload.accessToken;
      state.sid = action.payload.sid;
      state.refreshToken = action.payload.refreshToken;
      state.user = action.payload.userData;
      state.loading = false;
    },
    [authOperations.logIn.rejected]: setError,
    [authOperations.logOut.pending]: setLoading,
    [authOperations.logOut.fulfilled]: state => {
      state.isAuthenticated = false;
      state.accessToken = null;
      state.sid = null;
      state.refreshToken = null;
      state.user = null;
      state.loading = false;
    },
    [authOperations.logOut.rejected]: setError,
    [authOperations.getCurrentUser.pending]: setLoading,
    [authOperations.getCurrentUser.fulfilled]: (state, action) => {
      state.isAuthenticated = true;
      state.user = action.payload;
      state.loading = false;
    },
    [authOperations.getCurrentUser.rejected]: setError,
    [authOperations.updateUserBalance.pending]: setLoading,
    [authOperations.updateUserBalance.fulfilled]: (state, action) => {
      state.user.balance = action.payload.newBalance;
      state.loading = false;
    },
    [authOperations.updateUserBalance.rejected]: setError,
    [authOperations.refresh.pending]: setLoading,
    [authOperations.refresh.fulfilled]: (state, action) => {
      state.accessToken = action.payload.newAccessToken;
      state.sid = action.payload.newSid;
      state.refreshToken = action.payload.newRefreshToken;
      state.loading = false;
    },
    [authOperations.refresh.rejected]: state => {
      state.isAuthenticated = false;
      state.accessToken = null;
      state.sid = null;
      state.refreshToken = null;
      state.user = null;
      state.loading = false;
    },
  },
});

export default authSlice.reducer;
