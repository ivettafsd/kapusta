import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { format } from 'date-fns';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@mui/styles';
import { TextField, MenuItem, Button } from '@mui/material';
import transactionsOperations from '../../redux/transactionsOperations';
import { showErrorMessage } from '../../components/Toasters';
import { useActionsCreators } from '../../hooks/useActionsCreators';
import routes from '../../routes';
import sprite from '../../assets/common-sprite.svg';
import s from './AddTransactionForm.module.scss';

const useStyles = makeStyles({
  root: {
    '&.MuiFormControl-root': {
      width: '100%',
    },
    '& .Mui-focused.MuiFormLabel-root': {
      color: 'transparent',
    },
    '& .MuiFormLabel-root': {
      fontSize: '12px',
      transform: 'translate(15px, 50%)',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        border: 'none',
      },
    },
    '& .MuiOutlinedInput-input': {
      padding: '5px 15px',
      textAlign: 'left',
      fontSize: '12px',
    },
    '&.MuiButton-root': {
      borderRadius: '16px',
      width: '125px',
      padding: '12px',
      margin: '7px',
      fontSize: '12px',
      fontWeight: 700,
    },
  },
  rootTablet: {
    '&.MuiFormControl-root': {
      width: '170px',
      borderRight: '2px solid #f5f6fb',
    },
    '& .MuiFormLabel-root': {
      color: '#c7ccdc',
      transform: 'translate(15px, 15px)',
    },
    '& .MuiOutlinedInput-input': {
      padding: '10px 20px',
    },
    '&.MuiButton-root': {
      margin: '0 15px 0 0',
      boxShadow: 'none',
    },
    '&.MuiButton-root:last-child': {
      backgroundColor: 'transparent',
      outline: '2px solid #f5f6fb',
      border: 'none',
    },
    '&.MuiButton-root:hover': {
      boxShadow: 'none',
    },
    '&.MuiButton-root:active': {
      boxShadow: 'none',
      outline: 'none',
      border: 'none',
    },
  },
});

const AddTransactionForm = ({ type }) => {
  const classes = useStyles();
  const actions = useActionsCreators(transactionsOperations);
  const navigate = useNavigate();
  const incomeCategories = useSelector(
    (state) => state.transactions.incomeCategories,
  );
  const expenseCategories = useSelector(
    (state) => state.transactions.expenseCategories,
  );
  const { t } = useTranslation();

  const [categories, setCategories] = useState(expenseCategories);
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState('');
  const [sum, setSum] = useState('00.00');

  useEffect(() => {
    setCategories(type === 'income' ? incomeCategories : expenseCategories);
  }, [type, incomeCategories, expenseCategories]);

  const handleCleaner = () => {
    setDescription('');
    setCategory('');
    setSum('00.00');
  };

  const handleSubmit = async () => {
    if (description && category && Number(sum) !== 0) {
      const today = format(new Date(), 'yyyy-MM-dd');
      const amount = Math.round(Number(sum) * 100) / 100;
      const data = {
        description: description.trim(),
        amount: Math.abs(amount),
        date: today,
        category,
      };
      //transactionsOperations
      type === 'income'
        ? actions.createIncome(data)
        : actions.createExpense(data);
      navigate(routes.home);
      handleCleaner();
    } else {
      showErrorMessage(t('errors').allFields);
    }
  };

  return (
    <div className={s.form}>
      <div className={s.formBox}>
        <div className={s.inputs}>
          <input
            type="text"
            placeholder={t('transactionsForm').name}
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <TextField
            id="select-category"
            select
            className={
              window.innerWidth < 772
                ? classes.root
                : `${classes.root} ${classes.rootTablet}`
            }
            label={category ? null : t('transactionsForm').category}
            value={category}
            onChange={(e) => setCategory(e.target.value)}
          >
            {categories &&
              categories.map((option, idx) => (
                <MenuItem key={idx} value={option}>
                  {t('categories')[option]}
                </MenuItem>
              ))}
          </TextField>
        </div>
        <div className={s.balanceBox}>
          <div className={s.balance}>
            <input
              onChange={(e) => setSum(e.target.value)}
              value={sum}
              type="number"
              min={0}
            ></input>
            <span className={s.currency}>UAH</span>
          </div>
          <div className={s.confirm}>
            <svg className={s.icon} width="20" height="20">
              <use href={`${sprite}#icon-calculator`}></use>
            </svg>
          </div>
        </div>
      </div>
      <div className={s.btns}>
        <Button
          className={
            window.innerWidth < 772
              ? classes.root
              : `${classes.root} ${classes.rootTablet}`
          }
          color="primary"
          variant="contained"
          fullWidth
          type="button"
          onClick={handleSubmit}
        >
          {t('transactionsForm').input}
        </Button>
        <Button
          className={
            window.innerWidth < 772
              ? classes.root
              : `${classes.root} ${classes.rootTablet}`
          }
          color="secondary"
          variant="contained"
          fullWidth
          type="button"
          onClick={handleCleaner}
        >
          {t('transactionsForm').clear}
        </Button>
      </div>
    </div>
  );
};
export default AddTransactionForm;
