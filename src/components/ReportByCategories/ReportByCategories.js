import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import sprite from '../../assets/common-sprite.svg';
import { icons } from '../../constants';
import s from './ReportByCategories.module.scss';

const ReportByCategories = ({ selectedCategory, type, onSelectedCategory }) => {
  const expensesByPeriod = useSelector(
    state => state.transactions.expensesByPeriod,
  );
  const incomesByPeriod = useSelector(
    state => state.transactions.incomesByPeriod,
  );
  const { t } = useTranslation();
  const [report, setReport] = useState();

  useEffect(() => {
    const report = type ? incomesByPeriod : expensesByPeriod;
    setReport(report);
    const category = report.length > 0 ? report[0][0] : null;
    onSelectedCategory(category);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const report = type ? incomesByPeriod : expensesByPeriod;
    setReport(report);
    const category = report.length > 0 ? report[0][0] : null;
    onSelectedCategory(category);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [incomesByPeriod, expensesByPeriod, type]);

  return (
    <div className={s.report}>
      {report &&
        report.map(r => (
          <div
            className={
              selectedCategory === r[0] ? `${s.card} ${s.cardActive}` : s.card
            }
            key={r[0]}
            onClick={() => onSelectedCategory(r[0])}
          >
            <p>{r[1].total.toFixed(2)}</p>

            <svg height="56" width="56">
              <use href={`${sprite}#${icons[r[0]]}`}></use>
            </svg>

            <p>{t('categories')[r[0]]}</p>
          </div>
        ))}
    </div>
  );
};

export default ReportByCategories;
