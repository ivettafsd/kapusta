import {
  register,
  signIn,
  getUser,
  getRefreshToken,
  updateBalance,
} from './auth';
import axios from 'axios';

jest.mock('axios');

describe('register', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        email: 'testIvetta@gmail.com',
      },
    };
  });

  test('valid value after work register', async () => {
    const testNewUser = {
      email: 'testIvetta@gmail.com',
      password: '12345678',
    };
    axios.post.mockReturnValue(response);
    const data = await register(testNewUser);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });

  let responseFail;
  beforeEach(() => {
    responseFail = {
      data: undefined,
    };
  });
  test('invalid value after work register', async () => {
    const testFailNewUser = {
      email: 'testIvettagmail.com',
      password: '123456',
    };
    axios.post.mockReturnValue(responseFail);
    const data = await register(testFailNewUser);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(responseFail);
  });
});

describe('signIn', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        accessToken:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI2MzllNjMyOWRiN2E4MTA4MTQwMzg4M2MiLCJzaWQiOiI2MzllNjMzZGRiN2E4MTA4MTQwMzg4M2UiLCJpYXQiOjE2NzEzMjQ0NzcsImV4cCI6MTY3MTMyODA3N30.zcsr-x2vVcI-KPP_AboTUZzpjQkYH_u0L8yY62gjiyo',
        refreshToken:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI2MzllNjMyOWRiN2E4MTA4MTQwMzg4M2MiLCJzaWQiOiI2MzllNjMzZGRiN2E4MTA4MTQwMzg4M2UiLCJpYXQiOjE2NzEzMjQ0NzcsImV4cCI6MTY3Mzk1MjQ3N30.EzzXDIIuw5fRXDQAymq04zqVBrHQM3bQKq98vDAbhFw',
        sid: '639e633ddb7a81081403883e',
        userData: {
          email: 'testIvetta@gmail.com',
          balance: 0,
          id: '639e6329db7a81081403883c',
          transactions: [],
        },
      },
    };
  });

  test('valid value after work signIn', async () => {
    const testUser = {
      email: 'testIvetta@gmail.com',
      password: '12345678',
    };
    axios.post.mockReturnValue(response);
    const data = await signIn(testUser);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });

  let responseFail;
  beforeEach(() => {
    responseFail = {
      data: undefined,
    };
  });
  test('invalid value after work signIn', async () => {
    const testFailUser = {
      email: 'testIvettagmail.com',
      password: '123456',
    };
    axios.post.mockReturnValue(responseFail);
    const data = await signIn(testFailUser);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(responseFail);
  });
});

describe('getUser', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        balance: 159200,
        email: 'ivaUAA@gmail.com',
        transactions: [
          {
            amount: 800,
            category: 'Алкоголь',
            date: '2023-01-02',
            description: 'Martini',
            _id: '63b22592db7a810814039b72',
          },
          {
            amount: 80000,
            category: 'З/П',
            date: '2023-01-02',
            description: 'Salary',
            _id: '63b225a3db7a810814039b73',
          },
        ],
      },
    };
  });

  test('valid value after work getUser', async () => {
    axios.get.mockReturnValue(response);
    const data = await getUser();
    expect(axios.get).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });
});

describe('getRefreshToken', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        newAccessToken:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1ZmMyMDg1YmQwOTM2NTI4MTA3Y2UyNzQiLCJzaWQiOiI1ZmMyZDJmY2UxZDIwNTA2NzAyYmRkMjIiLCJpYXQiOjE2MDY2MDM1MTYsImV4cCI6MTYwNjYwNzExNn0.rJ_QjU4KvA76H96RHsvOBChK0Vjbd0NmqjMxdQVJIXA',
        newRefreshToken:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1ZmMyMDg1YmQwOTM2NTI4MTA3Y2UyNzQiLCJzaWQiOiI1ZmMyZDJmY2UxZDIwNTA2NzAyYmRkMjIiLCJpYXQiOjE2MDY2MDM1MTYsImV4cCI6MTYwNjYwNzExNn0.rJ_QjU4KvA76H96RHsvOBChK0Vjbd0NmqjMxdQVJIXB',
        newSid: '507f1f77bcf86cd799439011',
      },
    };
  });

  test('valid value after work getRefreshToken', async () => {
    axios.post.mockReturnValue(response);
    const testRefreshToken = {
      refreshToken:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1ZmMyMDg1YmQwOTM2NTI4MTA3Y2UyNzQiLCJzaWQiOiI1ZmMyZDJmY2UxZDIwNTA2NzAyYmRkMjIiLCJpYXQiOjE2MDY2MDM1MTYsImV4cCI6MTYwNjYwNzExNn0.rJ_QjU4KvA76H96RHsvOBChK0Vjbd0NmqjMxdQVJIXB',
      sid: '507f1f77bcf86cd799439011',
    };
    const data = await getRefreshToken(testRefreshToken);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });

  let responseFail;
  beforeEach(() => {
    responseFail = {
      data: undefined,
    };
  });
  test('invalid value after work getRefreshToken', async () => {
    const testFailRefreshToken = {
      refreshToken:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1ZmMyMDg1YmQwOTM2NTI4MTA3Y2UyNzQiLCJzaWQiOiI1ZmMyZDJmY2UxZDIwNTA2NzAyYmRkMjIiLCJpYXQiOjE2MDY2MDM1MTYsImV4cCI6MTYwNjYwNzExNn0.rJ_QjU4KvA76H96RHsvOBChK0Vjbd0NmqjMxdQVJIXB',
      did: '507f1f77bcf86cd799439011',
    };
    axios.post.mockReturnValue(responseFail);
    const data = await getRefreshToken(testFailRefreshToken);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(responseFail);
  });
});

describe('updateBalance', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        newBalance: 90000,
      },
    };
  });

  test('valid value after work updateBalance', async () => {
    const testBalance = {
      newBalance: 1,
    };
    axios.patch.mockReturnValue(response);
    const data = await updateBalance(testBalance);
    expect(axios.patch).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });

  let responseFail;
  beforeEach(() => {
    responseFail = {
      data: undefined,
    };
  });
  test('invalid value after work updateBalance', async () => {
    const testFailBalance = {
      balance: 'wwwww',
    };
    axios.patch.mockReturnValue(responseFail);
    const data = await updateBalance(testFailBalance);
    expect(axios.patch).toBeCalledTimes(1);
    expect(data).toEqual(responseFail);
  });
});
