import React, { useMemo } from 'react';
import Media from 'react-media';
import {
  VictoryChart,
  VictoryBar,
  VictoryLabel,
  VictoryAxis,
  VictoryContainer,
} from 'victory';
import s from './ChartBar.module.scss';

const ChartBar = ({ data }) => {
  const transactions = useMemo(() => {
    return data.sort((a, b) => b.earnings - a.earnings);
  }, [data]);

  const getLabels = (data) => {
    return data.map((datum) => `${datum.earnings}`);
  };

  const labels = useMemo(() => getLabels(data), [data]);

  return (
    <div className={s.chartbar}>
      <Media
        queries={{
          small: '(max-width: 771px)',
          medium: '(min-width: 772px) and (max-width: 1279px)',
          large: '(min-width: 1280px)',
        }}
      >
        {(matches) => (
          <>
            {matches.small && (
              <VictoryChart
                animate={{
                  duration: 1000,
                  onLoad: { duration: 1000 },
                }}
                containerComponent={<VictoryContainer />}
              >
                <VictoryAxis
                  style={
                    window.innerWidth < 772 && {
                      axis: { stroke: 'transparent' },
                      ticks: { stroke: 'transparent' },
                      tickLabels: { fill: 'transparent' },
                    }
                  }
                />
                <VictoryBar
                  horizontal
                  cornerRadius={{ top: 10, bottom: 0 }}
                  style={{
                    data: {
                      fill: '#ff751d',
                      width: 15,
                      strokeLinejoin: 'round',
                    },

                    labels: {
                      fill: '#52555F',
                      fontSize: 10,
                      fontWeight: 400,
                    },
                  }}
                  labelComponent={<VictoryLabel x={40} />}
                  labels={labels}
                  data={transactions}
                  x="quarter"
                  y="earnings"
                />
              </VictoryChart>
            )}
            {matches.medium && (
              <VictoryChart
                animate={{
                  duration: 1000,
                  onLoad: { duration: 1000 },
                }}
                containerComponent={<VictoryContainer />}
              >
                <VictoryAxis
                  style={{
                    axis: {
                      stroke: 'transparent',
                    },
                    tickLabels: {
                      fill: 'transparent',
                      fontSize: 12,
                      fontWeight: 400,
                    },
                    grid: {
                      fill: '#F5F6FB',
                      stroke: '#F5F6FB',
                    },
                  }}
                  dependentAxis
                />
                <VictoryAxis
                  tickFormat={(x) => x}
                  style={{
                    axis: {
                      stroke: '#F5F6FB',
                    },
                    tickLabels: {
                      fill: 'rgb(82, 85, 95)',
                      fontSize: '9px',
                      fontWeight: 100,
                    },
                  }}
                />
                <VictoryBar
                  cornerRadius={{ top: 10, bottom: 0 }}
                  style={{
                    data: {
                      fill: '#ff751d',
                      width: 25,
                      strokeLinejoin: 'round',
                    },

                    labels: {
                      fill: '#52555F',
                      fontSize: '9px',
                      fontWeight: 100,
                    },
                  }}
                  labelComponent={<VictoryLabel />}
                  labels={labels}
                  data={data}
                  x="quarter"
                  y="earnings"
                />
              </VictoryChart>
            )}
            {matches.large && (
              <VictoryChart
                animate={{
                  duration: 1000,
                  onLoad: { duration: 1000 },
                }}
                containerComponent={<VictoryContainer />}
              >
                <VictoryAxis
                  style={{
                    axis: {
                      stroke: 'transparent',
                    },
                    tickLabels: {
                      fill: 'transparent',
                      fontSize: 12,
                      fontWeight: 600,
                      fontFamily: 'Montserrat',
                    },
                    grid: {
                      fill: '#F5F6FB',
                      stroke: '#F5F6FB',
                    },
                  }}
                  dependentAxis
                />
                <VictoryAxis
                  tickFormat={(x) => x}
                  style={{
                    axis: {
                      stroke: '#F5F6FB',
                    },
                    tickLabels: {
                      fill: 'rgb(82, 85, 95)',
                      fontSize: '9px',
                      fontWeight: 100,
                    },
                  }}
                />
                <VictoryBar
                  cornerRadius={{ top: 10, bottom: 0 }}
                  style={{
                    data: {
                      fill: '#ff751d',
                      width: 25,
                      strokeLinejoin: 'round',
                    },

                    labels: {
                      fill: '#52555F',
                      fontSize: '9px',
                      fontWeight: 100,
                    },
                  }}
                  labelComponent={<VictoryLabel />}
                  labels={labels}
                  data={data}
                  x="quarter"
                  y="earnings"
                />
              </VictoryChart>
            )}
          </>
        )}
      </Media>
    </div>
  );
};
export default ChartBar;
