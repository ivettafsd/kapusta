import React from 'react';
import { useState, useEffect } from 'react';
import Media from 'react-media';
import { useSelector } from 'react-redux';
import HomeBtnsType from '../components/HomeBtnsType';
import ContainerContent from '../containers/ContainerContent';
import HomeBtnsMobile from '../components/HomeBtnsMobile';
import AddTransactionForm from '../components/AddTransactionForm';
import DateToday from '../components/DateToday';
import Table from '../components/Table';
import Resume from '../components/Resume';
import Balance from '../components/Balance';
import ButtonToReport from '../components/ButtonToReport';
import s from './HomeView.module.scss';

const HomeView = () => {
  const incomeCategories = useSelector(
    (state) => state.transactions.incomeCategories,
  );
  const expenseCategories = useSelector(
    (state) => state.transactions.expenseCategories,
  );
  const income = useSelector((state) => state.transactions.income);
  const expense = useSelector((state) => state.transactions.expense);

  const [widthScreen, setWidthScreen] = useState(window.innerWidth);
  const [selectedType, setSelectedType] = useState();
  const [categories, setCategories] = useState(null);

  useEffect(() => {
    if (selectedType === 'expense') {
      setCategories(expenseCategories);
    } else if (selectedType === 'income') {
      setCategories(incomeCategories);
    } else {
      setCategories(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedType, income, expense]);

  useEffect(() => {
    if (widthScreen < 772) {
      setSelectedType('all');
    } else {
      setSelectedType('expense');
    }
  }, [widthScreen]);

  useEffect(() => {
    window.addEventListener('resize', updateWidth);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return () => {
      window.removeEventListener('resize', updateWidth);
    };
    // eslint-disable-next-line
  }, []);

  const updateWidth = () => {
    if (widthScreen !== window.innerWidth) {
      setWidthScreen(window.innerWidth);
    }
  };

  return (
    <div className={s.contentFixed}>
      <ContainerContent>
        <div className={s.content}>
          <div className={s.balanceReportBox}>
            <ButtonToReport />
            <Balance />
          </div>
          <Media query="(max-width: 771px)" render={() => <DateToday />} />
          <div className={s.boxTransactions}>
            <Media
              query="(min-width: 772px)"
              render={() => (
                <HomeBtnsType type={selectedType} onClick={setSelectedType} />
              )}
            />
            <div className={s.transactionsContent}>
              <Media
                query="(min-width: 772px)"
                render={() => (
                  <>
                    {categories && (
                      <div className={s.header}>
                        <DateToday />
                        <AddTransactionForm type={selectedType} />
                      </div>
                    )}
                  </>
                )}
              />
              <div className={s.boxDesktop}>
                <Table type={selectedType} />

                <Media
                  query="(min-width: 1280px)"
                  render={() => <Resume type={selectedType} />}
                />
              </div>
            </div>
          </div>
          <Media
            query="(min-width: 772px) and (max-width: 1279px)"
            render={() => <Resume type={selectedType} />}
          />
        </div>
      </ContainerContent>
      <HomeBtnsMobile />
    </div>
  );
};

export default HomeView;
