import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import routes from '../../routes';
import s from './HomeBtnsMobile.module.scss';

const HomeBtnsMobile = () => {
  const { t } = useTranslation();
  return (
    <div className={s.btns}>
      <Link
        to={routes.addTransaction}
        state={{ typeTransaction: 'expense' }}
        className={s.btn}
      >
        {t('home').expense}
      </Link>
      <Link
        to={routes.addTransaction}
        state={{ typeTransaction: 'income' }}
        className={s.btn}
      >
        {t('home').income}
      </Link>
    </div>
  );
};

export default HomeBtnsMobile;
