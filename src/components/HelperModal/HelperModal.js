import Modal from '@mui/material/Modal';
import { useTranslation } from 'react-i18next';
import s from './HelperModal.module.scss';

const HelperModal = ({ open, onClose }) => {
  const { t } = useTranslation();
  return (
    <Modal
      open={open}
      onClick={() => onClose(false)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <p className={s.helper}>
        <span>{t('helper').text1}</span>
        <span className={s.text}>{t('helper').text2}</span>
      </p>
    </Modal>
  );
};

export default HelperModal;
