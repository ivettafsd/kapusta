const routes = {
  home: '/home',
  report: '/report',
  auth: '/auth',
  addTransaction: '/addTransaction',
};

export default routes;
