import { Suspense, lazy, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import authOperations from '../../redux/authOperations';
import transactionsOperations from '../../redux/transactionsOperations';
import { useActionsCreators } from '../../hooks/useActionsCreators';
import PrivateRoute from '../../containers/PrivateRoute';
import PublicRoute from '../../containers/PublicRoute';
import Loader from '../Loader';
import routes from '../../routes';
import Header from '../Header';
import s from './App.module.scss';

const AuthorizationView = lazy(() =>
  import(
    '../../views/AuthorizationView' /* webpackChunkName: "authorization-view" */
  ),
);
const HomeView = lazy(() =>
  import('../../views/HomeView' /* webpackChunkName: "home-view" */),
);
const ReportView = lazy(() =>
  import('../../views/ReportView' /* webpackChunkName: "report-view" */),
);
const AddTransactionView = lazy(() =>
  import(
    '../../views/AddTransactionView' /* webpackChunkName: "addTransaction-view" */
  ),
);
function App() {
  const authActions = useActionsCreators(authOperations);
  const transactionsActions = useActionsCreators(transactionsOperations);
  const accessToken = useSelector(state => state.auth.accessToken);
  const token = accessToken || sessionStorage.getItem('token');

  useEffect(() => {
    if (token) {
      authActions.getCurrentUser();
      transactionsActions.fetchIncomeCategories();
      transactionsActions.fetchExpenseCategories();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  return (
    <>
      <Header />
      <div className={s.content}>
        <Suspense fallback={<Loader />}>
          <Routes>
            <Route
              path={routes.home}
              element={<PrivateRoute component={<HomeView />} />}
            />
            <Route
              path={routes.report}
              element={<PrivateRoute component={<ReportView />} />}
            />
            <Route
              path={routes.auth}
              element={<PublicRoute component={<AuthorizationView />} />}
            />
            <Route
              path={routes.addTransaction}
              element={<PrivateRoute component={<AddTransactionView />} />}
            />
            <Route
              path="*"
              element={<PublicRoute component={<AuthorizationView />} />}
            />
          </Routes>
        </Suspense>
      </div>
    </>
  );
}

export default App;
