import { useTranslation } from 'react-i18next';
import { transactionsTypes } from '../../constants';
import s from './HomeBtnsType.module.scss';

const HomeBtnsType = ({ type = 'expense', onClick }) => {
  const { t } = useTranslation();

  return (
    <>
      {transactionsTypes.map(defaultType => (
        <button
          key={defaultType}
          id={defaultType}
          type="button"
          onClick={() => onClick(defaultType)}
          className={
            type === defaultType ? `${s.btnType} ${s.btnTypeActive}` : s.btnType
          }
        >
          {t('home')[defaultType]}
        </button>
      ))}
    </>
  );
};
export default HomeBtnsType;
