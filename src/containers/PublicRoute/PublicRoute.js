import { Navigate, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import routes from '../../routes';

const PublicRoute = ({ component }) => {
  const location = useLocation();

  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

  const from = location.state?.from?.pathname || routes.home;

  if (isAuthenticated) {
    return <Navigate to={from} />;
  }

  return component;
};

export default PublicRoute;
