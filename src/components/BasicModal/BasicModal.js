import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@mui/styles';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import s from './BasicModal.module.scss';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  minWidth: 300,
  maxWidth: 380,
  bgcolor: 'background.paper',
  boxShadow: '10px 10px 30px rgba(82, 85, 95, 0.4)',
  borderRadius: '30px',
  padding: '20px 30px',
  outline: 'none',
  fontWeight: 500,
  fontSize: '14px',
  textAlign: 'center',
};
const useStyles = makeStyles({
  root: {
    '&.MuiButton-root': {
      borderRadius: '16px',
      width: '120px',
      padding: '12px',
      fontSize: '12px',
      fontWeight: 700,
      margin: '5px',
    },
  },
});

const BasicModal = ({ children, open, onClose, onConfirm }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        {children}
        <div className={s.btns}>
          <Button
            className={classes.root}
            color="primary"
            variant="contained"
            fullWidth
            type="button"
            onClick={onConfirm}
          >
            {t('modal').yes}
          </Button>
          <Button
            className={classes.root}
            color="secondary"
            variant="contained"
            fullWidth
            type="button"
            onClick={onClose}
          >
            {t('modal').no}
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default BasicModal;
