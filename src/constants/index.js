export const icons = {
  'З/П': 'icon-salary',
  'Доп. доход': 'icon-enveloup',
  Продукты: 'icon-products',
  Алкоголь: 'icon-glass',
  Развлечения: 'icon-airsnake',
  Здоровье: 'icon-hands',
  Транспорт: 'icon-car',
  'Всё для дома': 'icon-sofa',
  Техника: 'icon-tools',
  'Коммуналка и связь': 'icon-list',
  'Спорт и хобби': 'icon-clay',
  Образование: 'icon-book',
  Прочее: 'icon-ufo',
};

export const transactionsTypes = ['expense', 'income'];

export const LANGUAGE_STORAGE_KEY = 'LANGUAGE_KEY';
