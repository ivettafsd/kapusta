import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';
import ContainerContent from '../containers/ContainerContent';
import Title from '../components/Title';
import AuthorizationForm from '../components/AuthorizationForm';
import authOperations from '../redux/authOperations';
import { useActionsCreators } from '../hooks/useActionsCreators';
import s from './AuthorizationView.module.scss';

const AuthorizationView = () => {
  const location = useLocation();
  const actions = useActionsCreators(authOperations);
  const { refreshToken, accessToken, sid } = queryString.parse(location.search);

  useEffect(() => {
    if (accessToken) {
      actions.logInGoogle({ refreshToken, accessToken, sid });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [actions, refreshToken, accessToken]);

  return (
    <div className={s.content}>
      <div className={s.box}>
        <ContainerContent>
          <div className={s.boxDesktop}>
            <Title />
            <AuthorizationForm />
          </div>
        </ContainerContent>
      </div>
    </div>
  );
};

export default AuthorizationView;
