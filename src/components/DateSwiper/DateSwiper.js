import { useState } from 'react';
import { useAsync } from 'react-use';
import { useTranslation } from 'react-i18next';
import { useActionsCreators } from '../../hooks/useActionsCreators';
import transactionsOperations from '../../redux/transactionsOperations';
import sprite from '../../assets/common-sprite.svg';
import s from './DateSwiper.module.scss';

const DateSwiper = ({ onClear }) => {
  const actions = useActionsCreators(transactionsOperations);
  const { t } = useTranslation();
  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth() + 1;
  const [year, setYear] = useState(currentYear);
  const [month, setMonth] = useState(currentMonth);

  useAsync(async () => {
    const updateMonth = month >= 10 ? month : '0' + month;
    actions.fetchTransactionsByPeriod({
      date: `${year}-${updateMonth}`,
    });
  }, []);

  useAsync(async () => {
    const updateMonth = month >= 10 ? month : '0' + month;
    actions.fetchTransactionsByPeriod({
      date: `${year}-${updateMonth}`,
    });
  }, [year, month]);

  const handleDecrementMonth = () => {
    onClear();
    if (month !== 1) {
      setMonth(prev => prev - 1);
    } else {
      setMonth(12);
      setYear(prev => prev - 1);
    }
  };

  const handleIncrementMonth = () => {
    onClear();
    if (month !== 12) {
      setMonth(prev => prev + 1);
    } else {
      setMonth(1);
      setYear(prev => prev + 1);
    }
  };

  return (
    <div className={s.period}>
      <p>{t('report').period}</p>
      <div className={s.date}>
        <button onClick={handleDecrementMonth}>
          <svg className={s.iconLeft}>
            <use href={`${sprite}#icon-row`}></use>
          </svg>
        </button>
        <p>{`${t('months')[month]} ${year}`}</p>
        <button onClick={handleIncrementMonth}>
          <svg className={s.iconRight}>
            <use href={`${sprite}#icon-row`}></use>
          </svg>
        </button>
      </div>
    </div>
  );
};

export default DateSwiper;
