import { useState, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
// import Media from 'react-media';
import ContainerContent from '../containers/ContainerContent';
import ButtonBack from '../components/ButtonBack';
import DateSwiper from '../components/DateSwiper';
import Report from '../components/Report';
import ChartBar from '../components/ChartBar';
import TypeTransactionSwiper from '../components/TypeTransactionSwiper';
// import Balance from '../components/Balance';
import ReportByCategories from '../components/ReportByCategories';
import s from './ReportView.module.scss';

const ReportView = () => {
  const incomesByPeriod = useSelector(
    (state) => state.transactions.incomesByPeriod,
  );
  const expensesByPeriod = useSelector(
    (state) => state.transactions.expensesByPeriod,
  );
  const { t } = useTranslation();

  const [type, setType] = useState(false);
  const [incomes, setIncomes] = useState();
  const [expenses, setExpenses] = useState();
  const [selectedCategory, setSelectedCategory] = useState();

  useEffect(() => {
    setExpenses(expensesByPeriod);
    setIncomes(incomesByPeriod);
    const data = type ? incomesByPeriod : expensesByPeriod;
    const category = data.length > 0 ? data[0][0] : null;
    setSelectedCategory(category);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [incomesByPeriod, expensesByPeriod]);

  const handleClear = () => {
    setIncomes(null);
    setExpenses(null);
    setSelectedCategory(null);
  };

  const handleChangeType = () => {
    setType((prev) => !prev);
  };

  const handleSelectCategory = (category) => {
    setSelectedCategory((prev) => (prev !== category ? category : null));
  };

  const listTransactions = useMemo(() => {
    const data = type ? incomes : expenses;
    if (!data || data.length === 0) return null;

    const deriveListTransactions = (currentCategoriesTransactions) => {
      if (!currentCategoriesTransactions) return null;

      const { total, ...transactions } = currentCategoriesTransactions[1];
      return Object.entries(transactions)
        .sort((a, b) => a[1] - b[1])
        .map((tr) => ({
          quarter: tr[0],
          earnings: tr[1],
        }));
    };

    const currentCategoriesTransactions = data.find(
      (tr) => tr[0] === selectedCategory,
    );
    return deriveListTransactions(currentCategoriesTransactions);
  }, [type, incomes, expenses, selectedCategory]);

  return (
    <div>
      <ContainerContent>
        <div className={s.content}>
          <div className={s.dashboard}>
            <ButtonBack />
            <div className={s.balanceDateBox}>
              <DateSwiper onClear={handleClear} />
            </div>
          </div>
          <Report />
          <div className={s.categories}>
            <TypeTransactionSwiper type={type} onClick={handleChangeType} />
            <ReportByCategories
              selectedCategory={selectedCategory}
              type={type}
              onSelectedCategory={handleSelectCategory}
            />
          </div>
          {listTransactions ? (
            <ChartBar data={listTransactions} />
          ) : (
            t('home').notFound
          )}
        </div>
      </ContainerContent>
    </div>
  );
};

export default ReportView;
