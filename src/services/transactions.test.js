import {
  getIncomeCategories,
  getExpenseCategories,
  getIncome,
  getExpense,
  postIncome,
  postExpense,
  getTransactionsByPeriod,
  deleteTransaction,
} from './transactions';
import axios from 'axios';

jest.mock('axios');

describe('getIncomeCategories', () => {
  let response;
  beforeEach(() => {
    response = {
      data: ['З/П', 'Доп. доход'],
    };
  });

  test('valid value after work getIncomeCategories', async () => {
    axios.get.mockReturnValue(response);
    const data = await getIncomeCategories();
    expect(axios.get).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });
});

describe('getExpenseCategories', () => {
  let response;
  beforeEach(() => {
    response = {
      data: [
        'Продукты',
        'Алкоголь',
        'Развлечения',
        'Здоровье',
        'Транспорт',
        'Всё для дома',
        'Техника',
        'Коммуналка и связь',
        'Спорт и хобби',
        'Образование',
        'Прочее',
      ],
    };
  });

  test('valid value after work getExpenseCategories', async () => {
    axios.get.mockReturnValue(response);
    const data = await getExpenseCategories();
    expect(axios.get).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });
});

describe('getIncome', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        incomes: [
          {
            amount: 90000,
            category: 'З/П',
            date: '2023-01-02',
            description: 'Salary',
            _id: '63b23524db7a810814039b7b',
          },
        ],
        monthsStats: {
          Август: 'N/A',
          Апрель: 'N/A',
          Декабрь: 'N/A',
          Июль: 'N/A',
          Июнь: 'N/A',
          Май: 'N/A',
          Март: 'N/A',
          Ноябрь: 'N/A',
          Октябрь: 'N/A',
          Сентябрь: 'N/A',
          Февраль: 'N/A',
          Январь: 90000,
        },
      },
    };
  });

  test('valid value after work getIncome', async () => {
    axios.get.mockReturnValue(response);
    const data = await getIncome();
    expect(axios.get).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });
});

describe('getExpense', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        incomes: [
          {
            amount: 800,
            category: 'Алкоголь',
            date: '2023-01-02',
            description: 'Martini',
            _id: '63b23518db7a810814039b7a',
          },
        ],
        monthsStats: {
          Август: 'N/A',
          Апрель: 'N/A',
          Декабрь: 'N/A',
          Июль: 'N/A',
          Июнь: 'N/A',
          Май: 'N/A',
          Март: 'N/A',
          Ноябрь: 'N/A',
          Октябрь: 'N/A',
          Сентябрь: 'N/A',
          Февраль: 'N/A',
          Январь: 800,
        },
      },
    };
  });

  test('valid value after work getExpense', async () => {
    axios.get.mockReturnValue(response);
    const data = await getExpense();
    expect(axios.get).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });
});

describe('postIncome', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        newBalance: 209200,
        transaction: {
          amount: 30000,
          category: 'З/П',
          date: '2023-01-02',
          description: 'Bonus',
          _id: '63b238dfdb7a810814039b7c',
        },
      },
    };
  });

  test('valid value after work postIncome', async () => {
    const testNewIncome = {
      amount: 30000,
      category: 'З/П',
      date: '2023-01-02',
      description: 'Bonus',
    };
    axios.post.mockReturnValue(response);
    const data = await postIncome(testNewIncome);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });

  let responseFail;
  beforeEach(() => {
    responseFail = {
      data: undefined,
    };
  });
  test('invalid value after work postIncome', async () => {
    const testFailNewIncome = {
      amount: 30000,
      category: 'Алкоголь',
      date: '2023-01-02',
      description: 'Bonus',
    };
    axios.post.mockReturnValue(responseFail);
    const data = await postIncome(testFailNewIncome);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(responseFail);
  });
});

describe('postExpense', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        newBalance: 207700,
        transaction: {
          amount: 1500,
          category: 'Транспорт',
          date: '2023-01-02',
          description: 'Tickets',
          _id: '63b238dfdb7a810814039b7c',
        },
      },
    };
  });

  test('valid value after work postExpense', async () => {
    const testNewExpense = {
      amount: 1500,
      category: 'Транспорт',
      date: '2023-01-02',
      description: 'Tickets',
    };
    axios.post.mockReturnValue(response);
    const data = await postExpense(testNewExpense);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });

  let responseFail;
  beforeEach(() => {
    responseFail = {
      data: undefined,
    };
  });
  test('invalid value after work postExpense', async () => {
    const testFailNewExpense = {
      amount: 1500,
      category: 'З/П',
      date: '2023-01-02',
      description: 'Tickets',
    };
    axios.post.mockReturnValue(responseFail);
    const data = await postExpense(testFailNewExpense);
    expect(axios.post).toBeCalledTimes(1);
    expect(data).toEqual(responseFail);
  });
});

describe('getTransactionsByPeriod', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        expenses: {
          expenseTotal: 2300,
          expensesData: {
            Алкоголь: { total: 800, Martini: 800 },
            Транспорт: { total: 1500, Tickets: 1500 },
          },
        },
        incomes: {
          incomeTotal: 120000,
          incomesData: {
            'З/П': { total: 120000, Salary: 90000, Bonus: 30000 },
          },
        },
      },
    };
  });

  test('valid value after work getTransactionsByPeriod', async () => {
    const testDate = {
      date: '2023-01',
    };
    axios.get.mockReturnValue(response);
    const data = await getTransactionsByPeriod(testDate);
    expect(axios.get).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });
});

describe('deleteTransaction', () => {
  let response;
  beforeEach(() => {
    response = {
      data: {
        newBalance: 209200,
      },
    };
  });

  test('valid value after work deleteTransaction', async () => {
    const testIdTransaction = '63b23af5db7a810814039b7d';
    axios.delete.mockReturnValue(response);
    const data = await deleteTransaction(testIdTransaction);
    expect(axios.delete).toBeCalledTimes(1);
    expect(data).toEqual(response);
  });
});
