import { render, fireEvent } from '@testing-library/react';
import HomeBtnsType from './HomeBtnsType';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({
    t: key => key,
    i18n: { changeLanguage: jest.fn() },
  }),
}));

describe('HomeBtnsType', () => {
  test('renders HomeBtnsType component', async () => {
    const props = {
      type: 'expense',
      onClick: jest.fn(),
    };
    render(<HomeBtnsType {...props} />);

    const expensesBtn = document.querySelector('#expense');
    expect(expensesBtn).toBeInTheDocument();
    fireEvent.click(expensesBtn);

    const incomeBtn = document.querySelector('#income');
    expect(incomeBtn).toBeInTheDocument();
    fireEvent.click(incomeBtn);
  });
});
