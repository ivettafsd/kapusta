import { createAsyncThunk } from '@reduxjs/toolkit';
import { getCurrentUser } from './authOperations';
import {
  getIncome,
  getExpense,
  getIncomeCategories,
  getExpenseCategories,
  getTransactionsByPeriod,
  deleteTransaction,
  postIncome,
  postExpense,
} from '../services';

export const fetchIncome = createAsyncThunk(
  'transactions/fetchIncome',
  async function (_, { rejectWithValue }) {
    try {
      const response = await getIncome();
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);
export const fetchExpense = createAsyncThunk(
  'transactions/fetchExpense',
  async function (_, { rejectWithValue }) {
    try {
      const response = await getExpense();
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);
export const fetchIncomeCategories = createAsyncThunk(
  'transactions/fetchIncomeCategories',
  async function (_, { rejectWithValue }) {
    try {
      const response = await getIncomeCategories();
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);
export const fetchExpenseCategories = createAsyncThunk(
  'transactions/fetchExpenseCategories',
  async function (_, { rejectWithValue }) {
    try {
      const response = await getExpenseCategories();
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);
export const fetchTransactionsByPeriod = createAsyncThunk(
  'transactions/fetchTransactionsByPeriod',
  async function (date, { rejectWithValue }) {
    try {
      const response = await getTransactionsByPeriod(date);
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);

export const removeTransaction = createAsyncThunk(
  'transactions/removeTransaction',
  async function (id, { rejectWithValue, dispatch }) {
    try {
      const response = await deleteTransaction(id);
      dispatch(getCurrentUser());
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);

export const createIncome = createAsyncThunk(
  'transactions/createIncome',
  async function (income, { rejectWithValue, dispatch }) {
    try {
      const response = await postIncome(income);
      dispatch(getCurrentUser());
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);

export const createExpense = createAsyncThunk(
  'transactions/createExpense',
  async function (expense, { rejectWithValue, dispatch }) {
    try {
      const response = await postExpense(expense);
      dispatch(getCurrentUser());
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);

const transactionsOperations = {
  fetchIncome,
  fetchExpense,
  fetchIncomeCategories,
  fetchExpenseCategories,
  fetchTransactionsByPeriod,
  removeTransaction,
  createIncome,
  createExpense,
};

export default transactionsOperations;
