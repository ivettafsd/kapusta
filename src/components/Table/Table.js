import { useState, useEffect } from 'react';
import { format } from 'date-fns';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Icon } from '@blueprintjs/core';
import { useActionsCreators } from '../../hooks/useActionsCreators';
import transactionsOperations from '../../redux/transactionsOperations';
import sprite from '../../assets/common-sprite.svg';
import s from './Table.module.scss';

const Table = ({ type }) => {
  const { t } = useTranslation();
  const actions = useActionsCreators(transactionsOperations);
  const incomeCategories = useSelector(
    (state) => state.transactions.incomeCategories,
  );
  const expense = useSelector((state) => state.transactions.expense);
  const income = useSelector((state) => state.transactions.income);
  const allTransactions = useSelector(
    (state) => state.transactions.allTransactions,
  );
  const [transactions, setTransactions] = useState();
  const [isSortTransactionsBySum, setIsSortTransactionsBySum] = useState(false);
  const [isSortTransactionsByDate, setIsSortTransactionsByDate] =
    useState(true);

  useEffect(() => {
    if (transactions?.length > 0) {
      if (isSortTransactionsBySum) {
        setTransactions((prev) => {
          return [...prev].sort((a, b) => a.amount - b.amount);
        });
      } else {
        setTransactions((prev) => {
          return [...prev].sort((a, b) => b.amount - a.amount);
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSortTransactionsBySum]);

  useEffect(() => {
    if (transactions?.length > 0) {
      if (isSortTransactionsByDate) {
        setTransactions((prev) => {
          return [...prev].sort((a, b) => new Date(a.date) - new Date(b.date));
        });
      } else {
        setTransactions((prev) => {
          return [...prev].sort((a, b) => new Date(b.date) - new Date(a.date));
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSortTransactionsByDate]);

  useEffect(() => {
    if (type === 'expense') {
      if (expense) {
        setTransactions(expense.expenses);
      }
    } else if (type === 'income') {
      if (income) {
        setTransactions(income.incomes);
      }
    } else {
      setTransactions(allTransactions);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [income, expense, type]);

  return (
    <div className={s.boxTable}>
      {transactions && transactions.length > 0 ? (
        <table className={s.table}>
          <thead>
            <tr>
              <th
                className={s.date}
                onClick={() => setIsSortTransactionsByDate((prev) => !prev)}
              >
                {t('home').table.date}
                {isSortTransactionsByDate ? (
                  <Icon icon="arrow-down" className={s.row} size={12} />
                ) : (
                  <Icon icon="arrow-up" className={s.row} size={12} />
                )}
              </th>
              <th>{t('home').table.description}</th>
              <th>{t('home').table.category}</th>
              <th
                className={s.sum}
                onClick={() => setIsSortTransactionsBySum((prev) => !prev)}
              >
                {isSortTransactionsBySum ? (
                  <Icon icon="arrow-up" className={s.row} size={12} />
                ) : (
                  <Icon icon="arrow-down" className={s.row} size={12} />
                )}
                {t('home').table.sum}
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {incomeCategories &&
              transactions &&
              [...transactions].reverse().map((transaction) => (
                <tr key={transaction._id}>
                  <td className={s.date}>
                    {format(new Date(transaction.date), 'dd.MM.yyyy')}
                  </td>
                  <td valign="bottom">
                    <p className={s.title}>{transaction.description}</p>
                    <span className={s.dateMobile}>
                      {format(new Date(transaction.date), 'dd.MM.yyyy')}
                    </span>
                  </td>
                  <td valign="bottom">
                    {t('categories')[transaction.category]}
                  </td>
                  <td
                    valign="center"
                    className={
                      incomeCategories.includes(transaction.category)
                        ? `${s.amount} ${s.green}`
                        : `${s.amount} ${s.red}`
                    }
                  >
                    {incomeCategories.includes(transaction.category)
                      ? transaction.amount.toFixed(2)
                      : `-${transaction.amount.toFixed(2)}`}{' '}
                    <span className={s.currency}>{t('home').currency}</span>
                  </td>
                  <td className={s.boxDelete}>
                    <button
                      className={s.delete}
                      onClick={() => actions.removeTransaction(transaction._id)}
                    >
                      <svg width="18" height="18">
                        <use href={`${sprite}#icon-delete`}></use>
                      </svg>
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      ) : (
        <h4 className={s.notFound}>{t('home').notFound}</h4>
      )}
    </div>
  );
};

export default Table;
