import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#FF751D',
      contrastText: '#fff',
    },
    secondary: {
      main: '#F5F6FB',
      contrastText: '#52555F',
    },
  },
});

export default theme;
