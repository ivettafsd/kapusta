import * as React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useActionsCreators } from '../../hooks/useActionsCreators';
import ContainerContent from '../../containers/ContainerContent';
import Logo from '../Logo';
import LngSwitcher from '../LngSwitcher';
import BasicModal from '../BasicModal';
import authOperations from '../../redux/authOperations';
import sprite from '../../assets/common-sprite.svg';
import s from './Header.module.scss';

const Header = () => {
  const actions = useActionsCreators(authOperations);
  const user = useSelector(state => state.auth.user);
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  const { t } = useTranslation();

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleLogOut = () => {
    actions.logOut();
    setOpen(false);
  };

  return (
    <>
      <header className={s.header}>
        <ContainerContent>
          <div className={s.content}>
            <Logo />
            {isAuthenticated && user && (
              <div className={s.forAuth}>
                <div className={s.user}>{user.email[0].toUpperCase()}</div>
                <p className={s.name}>{user.email.split('@')[0]}</p>
              </div>
            )}

            <div className={s.forAuth}>
              <LngSwitcher />
              {isAuthenticated && user && (
                <button className={s.exit} onClick={handleOpen}>
                  <svg width="16" height="16">
                    <use href={`${sprite}#icon-logout`}></use>
                  </svg>
                  <p>{t('header').exit}</p>
                </button>
              )}
            </div>
          </div>
        </ContainerContent>
      </header>
      <BasicModal open={open} onClose={handleClose} onConfirm={handleLogOut}>
        <p>{t('header').exitAsk}</p>
      </BasicModal>
    </>
  );
};

export default Header;
