import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useActionsCreators } from '../../hooks/useActionsCreators';
import authOperations from '../../redux/authOperations';
import HelperModal from '../HelperModal';
import s from './Balance.module.scss';

const Balance = ({ hideConfirm = false }) => {
  const { pathname } = useLocation();
  const actions = useActionsCreators(authOperations);
  const user = useSelector((state) => state.auth.user);
  const { t } = useTranslation();
  const [showBalance, setShowBalance] = useState('00.00');
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (user) {
      if (user.balance === 0) {
        if (pathname === '/home') {
          setOpen(true);
        }
      } else if (user.balance > 0 && user.balance < 10) {
        const currentBalance = '0' + user.balance.toFixed(2);
        setShowBalance(currentBalance);
      } else {
        const currentBalance = user.balance.toFixed(2);
        setShowBalance(currentBalance);
      }
    }
  }, [user, pathname]);

  const validationBalance = (e) => {
    if (e.target.value.match(/^\d{1,9}\.\d{2,2}?$/) != null) {
      setShowBalance(e.target.value);
    }
  };

  const handleChangeBalance = () => {
    const newBalance = Math.abs(showBalance);
    actions.updateUserBalance({ newBalance });
  };

  return (
    <>
      {user && (
        <div className={s.balanceBox}>
          <p className={s.title}>{t('home').balance}</p>
          <div className={s.balanceBorder}>
            <div className={s.balance}>
              <input
                onChange={validationBalance}
                value={showBalance}
                readOnly={user.balance > 0}
              ></input>
              <span>{t('home').currency}</span>
            </div>
            {!hideConfirm && user.balance === 0 && (
              <button
                className={s.confirm}
                type="button"
                onClick={handleChangeBalance}
                disabled={Number(showBalance) === 0 || user.balance > 0}
              >
                {t('home').confirm}
              </button>
            )}
          </div>
          <HelperModal open={open} onClose={setOpen} />
        </div>
      )}
    </>
  );
};

export default Balance;
