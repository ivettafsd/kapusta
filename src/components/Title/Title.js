import Media from 'react-media';
import { useTranslation } from 'react-i18next';
import titleMobile from '../../assets/title-mobile.png';
import titleTablet from '../../assets/title-tablet.png';
import titleDesktop from '../../assets/title-desktop.png';
import s from './Title.module.scss';

const Title = () => {
  const { t } = useTranslation();

  return (
    <div className={s.container}>
      <Media
        queries={{
          small: '(max-width: 771px)',
          medium: '(min-width: 772px) and (max-width: 1279px)',
          large: '(min-width: 1280px)',
        }}
      >
        {(matches) => (
          <>
            {matches.small && (
              <img src={titleMobile} alt="Kapusta" width="183" height="46" />
            )}
            {matches.medium && (
              <img src={titleTablet} alt="Kapusta" width="306" height="77" />
            )}
            {matches.large && (
              <img src={titleDesktop} alt="Kapusta" width="377" height="120" />
            )}
          </>
        )}
      </Media>
      <h1 className={s.title}>{t('title')}</h1>
    </div>
  );
};

export default Title;
