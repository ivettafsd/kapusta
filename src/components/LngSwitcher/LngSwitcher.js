import { useState, useEffect } from 'react';
import { useAsync } from 'react-use';
import { useTranslation } from 'react-i18next';
import { Switch } from '@blueprintjs/core';
import { LANGUAGE_STORAGE_KEY } from '../../constants';
import s from './LngSwitcher.module.scss';

const LngSwitcher = () => {
  const [isEng, setIsEng] = useState(true);

  const { i18n } = useTranslation();

  useAsync(async () => {
    const currentLng = await localStorage.getItem(LANGUAGE_STORAGE_KEY);
    currentLng === 'en' ? setIsEng(true) : setIsEng(false);
  }, []);

  useEffect(() => {
    let lng = isEng ? 'en' : 'ua';
    i18n.changeLanguage(lng);
  }, [isEng, i18n]);

  return (
    <Switch
      innerLabel="ua"
      className={s.switcher}
      innerLabelChecked="en"
      checked={isEng}
      onChange={() => setIsEng(prev => !prev)}
    />
  );
};

export default LngSwitcher;
