import { useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  TextField,
  FormGroup,
  FormControlLabel,
  Button,
  OutlinedInput,
  InputAdornment,
  IconButton,
  FormHelperText,
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@mui/styles';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { register } from '../../services';
import authOperations from '../../redux/authOperations';
import { useActionsCreators } from '../../hooks/useActionsCreators';
// import GoogleBtn from '../GoogleBtn';
import s from './AuthorizationForm.module.scss';

const useStyles = makeStyles({
  root: {
    '& label .MuiFormControlLabel-root': {
      margin: 0,
    },
    '& .MuiFormControlLabel-label': {
      fontSize: '10px',
      width: '253px',
      marginBottom: '10px',
      marginRight: 'auto',
    },
    '& .MuiTextField-root': {
      marginRight: 0,
    },
    '& .MuiFormControl-root .MuiFormHelperText-root': {
      margin: 0,
    },
    '& .MuiOutlinedInput-root': {
      borderRadius: '30px',
      backgroundColor: '#F6F7FB',
      width: '265px',
      padding: '17px 20px',

      '& .MuiOutlinedInput-input': {
        padding: 0,
      },

      '& fieldset': {
        borderColor: '#F6F7FB',
      },
    },
    '&.MuiButton-root': {
      borderRadius: '16px',
      width: '125px',
      padding: '12px',
      fontSize: '12px',
      fontWeight: 700,
    },
    '&.MuiFormControlLabel-root.MuiFormControlLabel-labelPlacementTop': {
      marginLeft: '0 !important',
      marginRight: '0 !important',
    },
  },
});

const AuthorizationForm = () => {
  const classes = useStyles();
  const actions = useActionsCreators(authOperations);
  const { t } = useTranslation();

  const f = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email(t('errors').email)
        .required(t('errors').required),
      password: Yup.string()
        .min(8, t('errors').min8)
        .required(t('errors').required),
    }),
    validateOnChange: false,
    validateOnBlur: true,
    onSubmit: async (values) => {
      actions.logIn(values);
    },
  });

  const [values, setValues] = useState({
    password: '',
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    f.values.password = event.target.value;
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleClickRegister = async () => {
    const errors = await f.validateForm();

    if (Object.keys(errors).length === 0) {
      const response = await register(f.values);
      if (response?.data) {
        f.handleSubmit();
      }
    }
  };

  return (
    <div className={s.container}>
      {/* <GoogleBtn /> */}
      <form onSubmit={f.handleSubmit} className={s.form}>
        <FormGroup>
          <FormControlLabel
            label={t('auth').email}
            labelPlacement="top"
            className={classes.root}
            control={
              <TextField
                error={f.errors?.email ? true : false}
                id="email"
                name="email"
                className={classes.root}
                value={f.values.email}
                onChange={f.handleChange}
                placeholder="your@email.com"
                helperText={f.errors?.email ? f.errors?.email : ' '}
              />
            }
          />
          <div>
            <FormControlLabel
              label={t('auth').password}
              labelPlacement="top"
              className={classes.root}
              control={
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={values.showPassword ? 'text' : 'password'}
                  value={values.password}
                  placeholder={t('auth').password}
                  onChange={handleChange('password')}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {values.showPassword ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              }
            />
            <FormHelperText error>
              {f.errors?.password ? f.errors?.password : ' '}
            </FormHelperText>
          </div>
        </FormGroup>
        <div className={s.btns}>
          <Button
            className={classes.root}
            color="primary"
            variant="contained"
            fullWidth
            type="submit"
          >
            {t('auth').logIn}
          </Button>
          <Button
            className={classes.root}
            color="secondary"
            variant="contained"
            fullWidth
            type="button"
            onClick={handleClickRegister}
          >
            {t('auth').registration}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default AuthorizationForm;
