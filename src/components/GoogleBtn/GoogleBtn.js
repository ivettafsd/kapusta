import { useTranslation } from 'react-i18next';
import sprite from '../../assets/common-sprite.svg';
import s from './GoogleBtn.module.scss';

const GoogleBtn = () => {
  const { t } = useTranslation();
  return (
    <div className={s.container}>
      <p className={s.google}>{t('auth').googleText}</p>
      <a
        href={`https://kapusta-backend.goit.global/auth/google`}
        className={s.btn}
      >
        <svg className={s.icon} width="18" height="18">
          <use href={`${sprite}#icon-google`}></use>
        </svg>
        {t('auth').googleBtn}
      </a>
    </div>
  );
};

export default GoogleBtn;
