import s from './ContainerContent.module.scss';

const ContainerContent = ({ children }) => {
  return <div className={s.container}>{children}</div>;
};

export default ContainerContent;
