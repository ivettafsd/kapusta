import { render, screen } from '@testing-library/react';
import DateToday from './DateToday';

describe('DateToday', () => {
  it('renders DateToday component', () => {
    render(<DateToday />);
    expect(screen.getByText(/\d{2}.\d{2}.\d{4}/)).toBeInTheDocument();
  });
});
