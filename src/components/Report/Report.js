import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Loader from '../Loader';
import s from './Report.module.scss';

const Report = () => {
  const { t } = useTranslation();
  const transactionsByPeriod = useSelector(
    state => state.transactions.transactionsByPeriod,
  );
  const [report, setReport] = useState(transactionsByPeriod);

  useEffect(() => {
    setReport(transactionsByPeriod);
  }, [transactionsByPeriod]);

  return (
    <div className={s.report}>
      <div className={s.expenses}>
        <p>{t('home').expense}:</p>
        {report ? (
          <p className={s.total}>
            -<span>{report.expenses.expenseTotal.toFixed(2)}</span>
            {t('home').currency}
          </p>
        ) : (
          <Loader style={{ padding: 0, height: 'auto' }} />
        )}
      </div>
      <div className={s.incomes}>
        <p>{t('home').income}:</p>
        {report ? (
          <p className={s.total}>
            +<span>{report.incomes.incomeTotal.toFixed(2)}</span>{' '}
            {t('home').currency}
          </p>
        ) : (
          <Loader style={{ padding: 0, height: 'auto' }} />
        )}
      </div>
    </div>
  );
};

export default Report;
