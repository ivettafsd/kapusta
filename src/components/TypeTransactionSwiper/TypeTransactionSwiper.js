import { useTranslation } from 'react-i18next';
import sprite from '../../assets/common-sprite.svg';
import s from './TypeTransactionSwiper.module.scss';

const TypeTransactionSwiper = ({ type, onClick }) => {
  const { t } = useTranslation();
  return (
    <div className={s.type}>
      <button onClick={onClick}>
        <svg className={s.iconLeft}>
          <use href={`${sprite}#icon-row`}></use>
        </svg>
      </button>
      <p>
        {type
          ? t('transactionsForm').incomeBtn
          : t('transactionsForm').expensesBtn}
      </p>
      <button onClick={onClick}>
        <svg className={s.iconRight}>
          <use href={`${sprite}#icon-row`}></use>
        </svg>
      </button>
    </div>
  );
};

export default TypeTransactionSwiper;
