import axios from 'axios';
import { showSuccessMessage, showErrorMessage } from '../components/Toasters';

export const register = async (credentials) => {
  try {
    const data = await axios.post('/auth/register', credentials);
    showSuccessMessage('Register success');
    return data;
  } catch (error) {
    showErrorMessage(error.message);
    return null;
  }
};

export const signIn = async (credentials) => {
  try {
    const data = await axios.post('/auth/login', credentials);
    return data;
  } catch (error) {
    showErrorMessage(error.response.data.message);
    return error;
  }
};

export const signOut = async () => {
  const data = await axios.post('/auth/logout');
  return data;
};

export const getUser = async () => {
  const data = await axios.get('/user');
  return data;
};

export const getRefreshToken = async ({ refreshToken, sid }) => {
  const data = await axios.post('/auth/refresh', sid, {
    headers: {
      Authorization: `Bearer ${refreshToken}`,
    },
  });
  return data;
};

export const getGoogle = async () => {
  const headers = {
    accept: '*/*',
    'Access-Control-Allow-Origin': 'https://kapusta-backend.goit.global',
  };
  const { data } = await axios.get('/auth/google', { headers });
  return data;
};

export const updateBalance = async (balance) => {
  const data = await axios.patch('/user/balance', balance);
  return data;
};
